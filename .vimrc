set nu
"requires vim-gui-common and vim-runtime
syntax on
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab
